import {createApp} from "vue";

// Vue Router。
import router from "@/plugins/vue-router.js";

// Element Plus。
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
import "element-plus/theme-chalk/dark/css-vars.css";
import * as ElementPlusIconsVue from "@element-plus/icons-vue";

// App View。
import App from "@/App.vue";

let app = createApp(App);
app.use(router);
app.use(ElementPlus, {size: "small", zIndex: 3000});
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}
app.mount("#app");
