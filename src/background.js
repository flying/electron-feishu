"use strict"

import {app, protocol, BrowserWindow, globalShortcut, Menu, ipcMain} from "electron";
import {createProtocol} from "vue-cli-plugin-electron-builder/lib";

const path = require("path");

// 定义：是否作为开发环境。
const isDevelopment = process.env.NODE_ENV !== "production";

// 处理：协议 schema 必须在应用准备前注册。
protocol.registerSchemesAsPrivileged([
    {
        scheme: "app",
        privileges: {
            secure: true,
            standard: true
        }
    }
])

// 处理：禁用菜单栏。
// 说明：如果是在创建窗口时使用 autoHideMenuBar:false，则只会隐藏，但按下Alt键时会再次唤起。
Menu.setApplicationMenu(null);

// 自定义函数：创建主应用窗口。
async function createMainWindow() {
    // 定义：窗口，主应用。
    const mainWindow = new BrowserWindow({
        // 窗口宽度。
        width: 960,
        minWidth: 960,
        // 窗口高度。
        height: 640,
        minHeight: 640,
        // 图标标题。
        titleBarStyle: "hidden",
        icon: path.join(__dirname, "./assets/icon.ico"),
        title: "飞书",
        // 窗口阴影。
        hasShadow: true,
        // 默认隐藏窗口。
        show: true,
        webPreferences: {
            // Use pluginOptions.nodeIntegration, leave this alone
            // See nklayman.github.io/vue-cli-plugin-electron-builder/guide/security.html#node-integration for more info
            nodeIntegration: process.env.ELECTRON_NODE_INTEGRATION,
            contextIsolation: !process.env.ELECTRON_NODE_INTEGRATION
        }
    });

    // 处理：如果是开发模式，则加载URL；否则使用自定义协议的Schema。
    if (process.env.WEBPACK_DEV_SERVER_URL) {
        await mainWindow.loadURL(process.env.WEBPACK_DEV_SERVER_URL);
    } else {
        createProtocol("app");
        await mainWindow.loadURL("app://./index.html");
    }

    // 系统事件：准备显示。
    // mainWindow.on("ready-to-show", () => {
    //     mainWindow.show();
    // });

    // 自定义事件：关闭窗口。
    ipcMain.on("window-close", () => {
        mainWindow.close();
    });

    // 处理：注册快捷键。
    // 说明：当使用 Ctrl+Shift+I 或 Command+Shift+I 时，打开开发者工具。
    globalShortcut.register("CommandOrControl+Shift+i", () => {
        mainWindow.webContents.openDevTools()
    });
}

// 系统事件：所有窗口均关闭后。
// 处理：关闭所有窗口后自动退出。
app.on("window-all-closed", () => {
    // 在 MacOS 上，应用程序及其菜单栏通常保持活动状态，直到用户使用 Command+Q 强制退出。
    // 因此当所有窗口关闭后，需要手动针对 MacOS 做退出处理。
    if (process.platform !== "darwin") {
        app.quit()
    }
})

// 系统事件：应用激活。
app.on("activate", () => {
    // 在 MacOS 上，当点击 Dock 程序坞中的图标但没有其他窗口打开时，通常会在应用程序中重新创建一个窗口。
    if (BrowserWindow.getAllWindows().length === 0) {
        createMainWindow().then();
    }
})

// 系统事件：应用准备完毕。
// 处理：进行初始化操作，并创建浏览器窗口。
// 备注：某些API只能在此事件发生后使用。
app.on("ready", async () => {
    // 校验：如果是开发环境。
    if (isDevelopment && !process.env.IS_TEST) {
        // 处理：安装 Vue 开发者工具。
        // 说明：启动项目时，会尝试安装Vue开发者工具，由于墙的因素会卡柱尝试很久，同时也不会经常用到这个插件，故暂时将其注释。
        //      使用的话，需要在上方添加导入语句：import installExtension, {VUEJS3_DEVTOOLS} from "electron-devtools-installer";
        // try {
        //     await installExtension(VUEJS3_DEVTOOLS)
        // } catch (e) {
        //     console.error("Vue Devtools failed to install:", e.toString())
        // }
    }

    // 处理：创建窗口。
    createMainWindow().then();
})

// 处理：在开发模式下，应父进程的请求，干净地退出。
if (isDevelopment) {
    if (process.platform === "win32") {
        process.on("message", (data) => {
            if (data === "graceful-exit") {
                app.quit()
            }
        })
    } else {
        process.on("SIGTERM", () => {
            app.quit()
        })
    }
}
