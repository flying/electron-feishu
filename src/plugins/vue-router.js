import {createRouter, createWebHashHistory} from "vue-router";

import BaseLayout from "@/layouts/base-layout.vue";
import HomeLayout from "@/layouts/home-layout.vue";
import VideoLayout from "@/layouts/video-layout.vue";

const routes = [
    {
        name: "base-layout",
        path: "/",
        component: BaseLayout,
        redirect: "/home",
        children: [
            {
                name: "main-layout",
                path: "/home",
                component: HomeLayout,
                redirect: "/home/index",
                children: [
                    {name: "home-index", path: "/home/index", component: () => import("@/views/home/home-index.vue")},
                    {name: "component-button-example", path: "/component/button-example", component: () => import("@/views/component/button-example.vue")},
                    {name: "component-form-example", path: "/component/form-example", component: () => import("@/views/component/form-example.vue")},
                    {name: "component-badge-example", path: "/component/badge-example", component: () => import("@/views/component/badge-example.vue")},
                    {name: "component-card-example", path: "/component/card-example", component: () => import("@/views/component/card-example.vue")},
                    {name: "component-carousel-example", path: "/component/carousel-example", component: () => import("@/views/component/carousel-example.vue")},
                    {name: "component-skeleton-example", path: "/component/skeleton-example", component: () => import("@/views/component/skeleton-example.vue")},
                    {name: "component-dialog-example", path: "/component/dialog-example", component: () => import("@/views/component/dialog-example.vue")}
                ]
            },
            {
                name: "video-layout",
                path: "/video",
                component: VideoLayout,
                redirect: "/video/index",
                children: [
                    {name: "video-index", path: "/video/index", component: () => import("@/views/video/video-index.vue")},
                    {name: "video-create", path: "/video/create", component: () => import("@/views/video/video-create.vue")},
                    {name: "video-doc", path: "/video/doc", component: () => import("@/views/video/video-doc.vue")},
                    {name: "video-join", path: "/video/join", component: () => import("@/views/video/video-join.vue")},
                    {name: "video-phone", path: "/video/phone", component: () => import("@/views/video/video-phone.vue")},
                    {name: "video-transfer", path: "/video/transfer", component: () => import("@/views/video/video-transfer.vue")},
                    {name: "video-wait", path: "/video/wait", component: () => import("@/views/video/video-wait.vue")}
                ]
            }
        ]
    }
];

const router = createRouter({
    history: createWebHashHistory(),
    routes: routes
});

export default router;