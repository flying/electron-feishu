const {defineConfig} = require("@vue/cli-service");

module.exports = defineConfig({
    transpileDependencies: true,

    pluginOptions: {
        electronBuilder: {
            nodeIntegration: true,
            builderOptions: {
                productName: "飞书",
                copyright: "Copyright © 2022 张德龙 All Rights Reserved",
                nsis: {
                    allowToChangeInstallationDirectory: true,
                    oneClick: false
                },
                win: {
                    icon: "./public/favicon.ico"
                },
                mac: {
                    icon: "./public/favicon.ico",
                    darkModeSupport: true
                }
            }
        }
    }
});