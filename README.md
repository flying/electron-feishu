# electron-feishu

#### 介绍
Electron，仿飞书布局。

#### 项目说明
1. Electron
2. 集成 Vue、ElementUI（详细步骤参见 doc 目录）
3. 仿飞书布局
4. vue 三级路由导航跳转
5. 调用摄像头设备

#### 如何启动：
1. 参考 doc 目录中的环境要求；
2. 安装依赖：`yarn install`
3. 启动：`yarn run electron:serve`
4. 打包：`yarn run electron:build`

#### 项目截图
![01-摄像头设备](doc/img/01-camera.png)

![02-集成Element表单](doc/img/02-form.png)

![03-多级路由切换](doc/img/03-meeting.png)

![04-关闭提示](doc/img/04-confirm.png)

#### 墨菲安全
[![Security Status](https://www.murphysec.com/platform3/v31/badge/1672812299876589568.svg)](https://www.murphysec.com/console/report/1672809117462380544/1672812299876589568)
